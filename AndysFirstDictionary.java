import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by Patrik on 2014-10-13.
 */
class AndysFirstDictionary {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/AndysFirstDictionary.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<String> wordList = new ArrayList<String>();
        HashSet<String> usedWords = new HashSet<String>();

        String line;
        while ((line = input.readLine()) != null) {
            String[] words = filter(line.toLowerCase()).split(" ");

            for (String word : words) {
                if (word.isEmpty()) continue;

                if (!usedWords.contains(word)) {
                    wordList.add(word);
                    usedWords.add(word);
                }
            }
        }

        Collections.sort(wordList);
        for (String word : wordList) {
            System.out.println(word);
        }
    }

    private static String filter(String word) {
        return word.replaceAll("[^a-z]"," ");
    }
}
