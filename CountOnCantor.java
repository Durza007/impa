import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-04.
 */
class CountOnCantor {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/CountOnCantor.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        // Build table


        String line;
        while ((line = input.readLine()) != null) {
            int number = Integer.parseInt(line);

            int a = 1;
            int b = 1;
            boolean up = true;
            int count = 1;
            while (count != number) {
                if (up) {
                    if (a == 1) {
                        b++;
                        up = false;
                    } else {
                        b++;
                        a--;
                    }
                } else {
                    if (b == 1) {
                        a++;
                        up = true;
                    } else {
                        a++;
                        b--;
                    }
                }
                count++;
            }

            System.out.println(String.format("TERM %d IS %d/%d", number, a, b));
        }
    }
}
