import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Patrik on 2014-10-06.
 */
class StackingBoxes {
    private static class Box implements Comparable<Box> {
        final int id;
        final int[] size;
        List<Box> canHold;

        public Box(int id, int n) {
            this.id = id;
            this.size = new int[n];
            this.canHold = new LinkedList<Box>();
        }

        @Override
        public int compareTo(Box o) {
            return o.canHold.size() - this.canHold.size();
        }
    }

    private static class Path {
        final Box box;
        final Path parent;
        Path child;
        int count = 0;

        public Path(Box box, Path parent, int count) {
            this.box = box;
            this.parent = parent;
            this.count = count;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/StackingBoxes.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while ((line = input.readLine()) != null) {
            //long start = System.nanoTime();
            String[] params = line.split(" ");

            final int k = Integer.parseInt(params[0]);
            final int n = Integer.parseInt(params[1]);

            Box[] boxes = new Box[k];

            for (int box = 0; box < k; box++) {
                String[] size = input.readLine().split(" ");

                boxes[box] = new Box(box + 1, n);
                for (int i = 0; i < n; i++) {
                    boxes[box].size[i] = Integer.parseInt(size[i]);
                }

                Arrays.sort(boxes[box].size);
            }

            for (int i = 0; i < k; i++) {
                for (int j = i + 1; j < k; j++) {
                    final Box a = boxes[i];
                    final Box b = boxes[j];

                    if (fitsInside(a.size, b.size)) {
                        b.canHold.add(a);
                    }
                    if (fitsInside(b.size, a.size)) {
                        a.canHold.add(b);
                    }
                }
            }

            Arrays.sort(boxes);

            //System.out.println("Solve1: " + ((System.nanoTime() - start) / 1000000000.0));

            //start = System.nanoTime();
            //Path path = findSolution(boxes, new Path(boxes[0], null), 1);
            Path path =  new Path(boxes[0], null, 1);

            while (path.box.canHold.size() > 0) {
                Collections.sort(path.box.canHold);
                Box b = path.box.canHold.get(0);

                boolean inUse;
                while (true) {
                    Path p = path;
                    inUse = false;
                    while (p != null) {
                        if (p.box == b) {
                            inUse = true;
                            break;
                        }
                        p = p.parent;
                    }

                    if (inUse) {
                        path.box.canHold.remove(0);

                        if (path.box.canHold.size() == 0) break;
                        b = path.box.canHold.get(0);
                    } else {
                        break;
                    }
                }

                if (inUse) break;

                path = new Path(b, path, path.count + 1);
            }

            //System.out.println("Solve2: " + ((System.nanoTime() - start) / 1000000000.0));

            System.out.println(path.count);
            StringBuilder sb = new StringBuilder();
            while (path != null) {
                sb.append(path.box.id).append(' ');
                path = path.parent;
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());
        }
    }

    /*private static Path findSolution(Box[] boxes, Path path, int depth) {
        if (path.box.canHold.size() > 0) {
            path.count = depth;
            Path bestP = path;
            for (Box b : path.box.canHold) {
                boolean isIsUsed = false;
                Path t = path;
                do {
                    if (t.box == b) {
                        isIsUsed = true;
                        break;
                    }
                    t = t.parent;
                } while (t != null);

                if (isIsUsed) continue;

                Path p = findSolution(boxes, new Path(b, path), depth + 1);
                if (bestP == null || p.count > bestP.count) {
                    bestP = p;
                }
            }
            return bestP;
        } else {
            path.count = depth;
            return path;
        }
    }*/

    private static boolean fitsInside(int[] a, int[] b) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] >= b[i]) return false;
        }

        return true;
    }
}
