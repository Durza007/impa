import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by Patrik on 2014-10-07.
 */
class PolynomialRemains {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/PolynomialRemains.txt"));
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));

        while (true) {
            final int n = in.nextInt();
            final int k = in.nextInt();

            if (n == -1 && k == -1) break;

            int[] polynom = new int[n + 1];
            for (int index = 0; index < polynom.length; index++) {
                polynom[index] = in.nextInt();
            }

            int d = polynom.length - 1;
            for (int index = polynom.length - 1; index >= k; index--) {
                if (polynom[index] != 0) {
                    d = index - 1;
                    if (k > 0) {
                        polynom[index - k] -= polynom[index];
                    }

                    polynom[index] = 0;
                }
            }

            if (d < 0) d = 0;

            StringBuilder sb = new StringBuilder();
            for (int index = 0; index <= d; index++) {
                sb.append(polynom[index]).append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());
        }
    }
}
