import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Patrik on 2014-10-03.
 */
class DNASorting {
    private static class DNA implements Comparable<DNA> {
        String line;
        int sortNum;

        @Override
        public int compareTo(DNA other) {
            return this.sortNum - other.sortNum;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/DNASorting.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        int numSets = Integer.parseInt(input.readLine());

        for (int set = 0; set < numSets; set++) {
            if (set != 0) System.out.println();

            input.readLine();
            String[] sizes = input.readLine().split(" ");
            int n = Integer.parseInt(sizes[0]);
            int m = Integer.parseInt(sizes[1]);

            DNA[] list = new DNA[m];
            for (int line = 0; line < m; line++) {
                list[line] = new DNA();
                list[line].line = input.readLine();
            }

            // Find out all the sortedness values for all strings
            for (DNA dna : list) {
                boolean swapped;
                byte[] chars = dna.line.getBytes();
                do {
                    swapped = false;
                    for (int index = 0; index < n - 1; index++) {
                        if (chars[index] > chars[index + 1]) {
                            chars[index] ^= chars[index + 1];
                            chars[index + 1] ^= chars[index];
                            chars[index] ^= chars[index + 1];

                            swapped = true;
                            dna.sortNum++;
                        }
                    }
                } while (swapped);
            }

            Arrays.sort(list);

            StringBuffer sb = new StringBuffer((n + 1) * m);
            for (DNA dna : list) {
                sb.append(dna.line).append('\n');
            }

            System.out.print(sb.toString());
        }

        input.close();
    }
}
