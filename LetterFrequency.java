import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-06.
 */
class LetterFrequency {
    private static final byte A = (byte)'a';
    private static final byte Z = (byte)'z';

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/LetterFrequency.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            byte[] text = input.readLine().toLowerCase().replaceAll(" ", "").getBytes();

            int[] letters = new int[26];
            for (byte c : text) {
                if (c >= A && c <= Z) {
                    letters[c - A]++;
                }
            }

            int max = 0;
            for (int index = 0; index < letters.length; index++) {
                max = Math.max(max, letters[index]);
            }

            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < letters.length; index++) {
                if (letters[index] == max) {
                    sb.append((char)(index + A));
                }
            }

            System.out.println(sb.toString());
        }
    }
}
