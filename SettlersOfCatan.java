import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Patrik on 2014-10-14.
 */
class SettlersOfCatan {
    private static class Pos {
        public int x;
        public int y;

        public Pos() {};

        public Pos(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Pos add(Pos other) {
            this.x += other.x;
            this.y += other.y;
            return this;
        }
    }

    private final static Pos[] dirs = new Pos[] {
        new Pos(-1, 1),
        new Pos(-1, 0),
        new Pos(0, -1),
        new Pos(1, -1),
        new Pos(1, 0),
        new Pos(0, 1)
    };

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/SettlersOfCatan.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        byte[] numbers = new byte[10000];
        byte[][] board = new byte[150][150];
        int[] resourceCount = new int[6];
        Pos p = new Pos(board.length / 2, board.length / 2);

        board[p.x][p.y] = 1;

        p.add(dirs[4]);
        board[p.x][p.y] = 2;

        resourceCount[1] = 1;
        resourceCount[2] = 1;

        numbers[0] = 1;
        numbers[1] = 2;

        int count = 1;
        int lastDir = 4;
        while (++count < 10000) {
            // Find direction to go in
            for (int d = 0; d < dirs.length; d++) {
                int dir = ((lastDir + 2) - d) % 6;
                if (dir < 0) dir = dir + 6;

                if (board[p.x + dirs[dir].x][p.y + dirs[dir].y] == 0) {
                    p.add(dirs[dir]);
                    lastDir = dir;

                    List<Byte> possible = new LinkedList<Byte>(Arrays.asList((byte)1, (byte)2, (byte)3, (byte)4, (byte)5));

                    for (Pos neighbour : dirs) {
                        possible.remove((Byte)(board[p.x + neighbour.x][p.y + neighbour.y]));
                    }

                    if (possible.size() == 1) {
                        board[p.x][p.y] = possible.get(0);
                    } else {
                        byte minIndex = possible.get(0);

                        for (byte i : possible) {
                            if (i == minIndex) continue;
                            if (resourceCount[i] < resourceCount[minIndex]) {
                                minIndex = i;
                            }
                        }

                        board[p.x][p.y] = minIndex;
                        resourceCount[minIndex]++;
                    }

                    numbers[count] = board[p.x][p.y];
                    break;
                }
            }
        }

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            final int n = Integer.parseInt(input.readLine());

            System.out.println(numbers[n - 1]);
        }
    }
}
