import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-12.
 */
class CountTheFactors {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/CountTheFactors.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while ((line = input.readLine()) != null) {
            final int number = Integer.parseInt(line);
            if (number == 0) break;

            double b = 2.0;
            double lastTested = 0;
            int count = 0;
            double value = number;
            while (b <= value) {
                if ((value / b) == Math.round(value / b)) {
                    if (b != lastTested) {
                        count++;
                        lastTested = b;
                    }

                    value /= b;
                } else {
                    if (b == 2.0) {
                        b = 3.0;
                    } else {
                        b += 2.0;
                    }
                }
            }

            System.out.println(String.format("%d : %d", number, count));
        }
    }
}
