import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-11.
 */
class CopyingBooks {
    public static boolean USE_BRUTE = true;

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/CopyingBooks.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            String[] params = input.readLine().split(" ");
            final int m = Integer.parseInt(params[0]);
            final int k = Integer.parseInt(params[1]);

            if (k == 1) {
                System.out.println(input.readLine().trim());
                continue;
            }

            int[] books = new int[m];
            String[] pageNums = input.readLine().split(" ");
            for (int index = 0; index < m; index++) {
                books[index] = Integer.parseInt(pageNums[index]);
            }

            System.out.println(solve(m, k, books));
        }
    }

    public static String solve(int m, int k, int[] books) {
        long[] sum = new long[m + 1];
        for (int index = 0; index < m; index++) {
            sum[index + 1] = sum[index] + books[index];
        }

        int[][] scribers = new int[k][2];
        int[] bestSolution = new int[k];
        bestSolution[k - 1] = m;
        for (int index = 0; index < k - 1; index++) {
            scribers[index][0] = index;
            scribers[index][1] = index + 1;
        }
        scribers[k - 1][0] = k - 1;
        scribers[k - 1][1] = m;

        if (USE_BRUTE) {
            bruteForce(sum, bestSolution, k);
        } else {
            long avarage = sum[sum.length - 1] / k;
            int offTarget = 0;
            //System.out.println("Avg: " + avarage);
            for (int index = 0; index < k - 1; index++) {
                int start;
                if (index == 0) {
                    start = 0;
                } else {
                    start = bestSolution[index - 1];
                }
                for (int nums = start; nums <= m - (k - index - 1); nums++) {
                    if (sum[nums] - sum[start] > (avarage + offTarget) && nums > start + 1) {
                        offTarget += avarage - (sum[nums - 1] - sum[start]);
                        bestSolution[index] = nums - 1;
                        break;
                    } else {
                        bestSolution[index] = nums;
                    }
                }
            }

            //System.out.println("Prelim1: " + getSolutionString(m, books, bestSolution));
            groupTogether(k, sum, bestSolution);

            //System.out.println("Prelim2: " + getSolutionString(m, books, bestSolution));

            newMoveToRight(k, sum, bestSolution);
            //while (redistribute(k, sum, bestSolution, avarage));
        }

        //System.out.println("Final: " + getSolutionString(m, books, bestSolution));

        return getSolutionString(m, books, bestSolution);
    }

    public static void groupTogether(int k, long[] sum, int[] bestSolution) {
        long minTime = getMinTime(sum, bestSolution);
        boolean improved;
        do {
            improved = false;
            boolean changed;
            do {
                int start = 0;
                int worstIndex = 0;
                for (int index = 0; index < k; index++) {
                    if (sum[bestSolution[index]] - sum[start] == minTime) {
                        if (bestSolution[index] - start > 1) {
                            worstIndex = index;
                            break;
                        }
                    }

                    start = bestSolution[index];
                }

                changed = false;
                if (worstIndex != 0) {
                    start = 0;
                    boolean brokeUpWorst = false;
                    for (int index = 0; index < k - 1; index++) {
                        while (sum[bestSolution[index] + 1] - sum[start] < minTime) {
                            bestSolution[index]++;
                            changed = true;
                            if (bestSolution[index + 1] == bestSolution[index]) {
                                if (index < worstIndex) {
                                    for (int i = index + 1; i < worstIndex - 1; i++) {
                                        bestSolution[i] = bestSolution[i + 1];
                                    }
                                    bestSolution[worstIndex - 1] = bestSolution[worstIndex] - 1;
                                    permutateRegion(sum, bestSolution, worstIndex - 1, worstIndex, minTime);
                                } else {
                                    for (int i = index - 1; i >= worstIndex; i--) {
                                        bestSolution[i + 1] = bestSolution[i];
                                    }
                                    bestSolution[worstIndex] = bestSolution[worstIndex + 1] - 1;
                                    permutateRegion(sum, bestSolution, worstIndex, worstIndex + 1, minTime);
                                }
                                brokeUpWorst = true;
                                break;
                            } else if (index + 1 == worstIndex) {
                                brokeUpWorst = true;
                                break;
                            }
                        }

                        if (brokeUpWorst) break;
                        start = bestSolution[index];
                    }
                } else {
                    break;
                }

                long newMinTime = getMinTime(sum, bestSolution);
                improved = improved || newMinTime < minTime;
                minTime = newMinTime;
            } while (changed);

            do {
                int start = 0;
                int worstIndex = -1;
                for (int index = 0; index < k; index++) {
                    if (sum[bestSolution[index]] - sum[start] == minTime) {
                        if (bestSolution[index] - start > 1) {
                            worstIndex = index;
                            break;
                        }
                    }

                    start = bestSolution[index];
                }

                if (worstIndex == 0 && bestSolution[0] == 1) break;

                changed = false;
                if (worstIndex != -1) {
                    boolean brokeUpWorst = false;
                    for (int index = k - 2; index >= 0; index--) {
                        while (sum[bestSolution[index + 1]] - sum[bestSolution[index] - 1] < minTime) {
                            if (bestSolution[index] == 1) break;
                            bestSolution[index]--;
                            changed = true;
                            if (index != 0 && bestSolution[index - 1] == bestSolution[index]) {
                                if (index < worstIndex) {
                                    for (int i = index; i < worstIndex - 1; i++) {
                                        bestSolution[i] = bestSolution[i + 1];
                                    }
                                    bestSolution[worstIndex - 1] = bestSolution[worstIndex] - 1;
                                    permutateRegion(sum, bestSolution, worstIndex - 1, worstIndex, minTime);
                                } else {
                                    for (int i = index - 1; i >= worstIndex; i--) {
                                        bestSolution[i + 1] = bestSolution[i];
                                    }
                                    bestSolution[worstIndex] = bestSolution[worstIndex + 1] - 1;
                                    permutateRegion(sum, bestSolution, worstIndex, worstIndex + 1, minTime);
                                }
                                brokeUpWorst = true;
                                break;
                            } else if (index - 1 == worstIndex) {
                                brokeUpWorst = true;
                                break;
                            }
                        }

                        if (brokeUpWorst) break;
                    }
                } else {
                    break;
                }

                long newMinTime = getMinTime(sum, bestSolution);
                improved = improved || newMinTime < minTime;
                minTime = newMinTime;
            } while (changed);
        } while (improved);
    }

    public static void newMoveToRight(int k, long[] sum, int[] solution) {
        long minTime = getMinTime(sum, solution);
        boolean changed;
        do {
            int placeToInsert = -1;
            int start = 0;
            for (int index = 0; index < k; index++) {
                if (solution[index] - start > 1) {
                    placeToInsert = index;
                    break;
                }

                start = solution[index];
            }

            if (placeToInsert == -1) break;

            changed = false;
            for (int index = k - 1; index > placeToInsert; index--) {
                if (sum[solution[index]] - sum[solution[index - 1] - 1] <= minTime) {
                    solution[index - 1]--;
                    changed = true;
                    if (index != placeToInsert + 1 && solution[index - 1] == solution[index - 2]) {
                        for (int i = index - 2; i >= placeToInsert; i--) {
                            solution[i + 1] = solution[i];
                        }
                        solution[placeToInsert] = solution[placeToInsert + 1] - 1;
                        break;
                    }
                }
            }
        } while (changed);
    }

    public static String getSolutionString(int m, int[] books, int[] solution) {
        int scriber = 0;
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < m; index++) {
            if (solution[scriber] == index) {
                sb.append('/').append(' ');
                scriber++;
            }

            sb.append(books[index]).append(' ');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    private static void permutateRegion(long[] sum, int[] solution, int start, int end, long currentMin) {
        int[] best = new int[end - start + 1];
        int iStart = 0;
        if (start > 0) {
            iStart = solution[start - 1];
        }
        for (int i = start; i <= end; i++) {
            best[i - start] = solution[i];
            solution[i] = ++iStart;
        }
        solution[solution.length - 1] = sum.length - 1;

        long minSum = currentMin;
        while (true) {
            // Find current work time
            long minTime = 0;
            iStart = 0;
            if (start > 0) {
                iStart = solution[start - 1];
            }
            for (int i = start; i <= end + 1; i++) {
                if (i >= solution.length) continue;
                minTime = Math.max(minTime, sum[solution[i]] - sum[iStart]);
                iStart = solution[i];
            }
            if (minTime < minSum) {
                minSum = minTime;
                for (int i = start; i <= end; i++) {
                    best[i - start] = solution[i];
                }
            }

            // Iterate
            int scriber = end;
            while (scriber >= start) {
                if ((scriber + 1 != solution.length && solution[scriber] != solution[scriber + 1] - 1) ||
                    (scriber + 1 == solution.length && solution[scriber] != sum.length - 1)) {
                    solution[scriber]++;

                    for (int i = scriber; i < end; i++) {
                        solution[i + 1] = solution[i] + 1;
                    }
                    solution[solution.length - 1] = sum.length - 1;
                    break;
                } else {
                    scriber--;
                }
            }

            if (scriber == start - 1) break;
        }

        for (int i = start; i <= end; i++) {
            solution[i] = best[i - start];
        }
    }

    private static long getMinTime(long[] sum, int[] solution) {
        int start = 0;
        long minTime = 0;
        for (int index = 0; index < solution.length; index++) {
            minTime = Math.max(minTime, sum[solution[index]] - sum[start]);
            start = solution[index];
        }

        return minTime;
    }

    private static void bruteForce(long[] sum, int[] bestSolution, int k) {
        int[] solution = new int[k];
        solution[k - 1] = sum.length - 1;
        for (int index = 0; index < k - 1; index++) {
            solution[index] = index + 1;
        }

        System.arraycopy(solution, 0, bestSolution, 0, k);

        long minTime = getMinTime(sum, solution);
        while (true) {
            long newMinTime = getMinTime(sum, solution);
            if (newMinTime < minTime) {
                minTime = newMinTime;
                System.arraycopy(solution, 0, bestSolution, 0, k);
            }

            // Iterate
            int scriber = k - 2;
            while (scriber >= 0) {
                if (solution[scriber] != solution[scriber + 1] - 1) {
                    solution[scriber]++;
                } else {
                    scriber--;
                    continue;
                }
                if ((scriber != 0 && sum[solution[scriber]] - sum[solution[scriber - 1]] >= minTime) ||
                    (scriber == 0 && sum[solution[scriber]] >= minTime)) {
                    scriber--;
                    continue;
                }

                for (int index = scriber + 1; index < k - 1; index++) {
                    solution[index] = solution[index - 1] + 1;
                }
                break;
            }

            if (scriber == -1) break;
        }
    }
}
