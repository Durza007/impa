import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Patrik on 2014-10-06.
 */
class IDCodes {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/IDCodes.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while ((line = input.readLine()).equals("#") == false) {
            byte[] raw = line.getBytes();
            byte[] word = line.getBytes();
            Arrays.sort(raw);

            // Check if there are no successions
            boolean done = true;
            for (int index = 0; index < raw.length; index++) {
                if (raw[index] != word[word.length - index - 1]) {
                    done = false;
                    break;
                }
            }

            if (done) {
                System.out.println("No Successor");
                continue;
            }

            List<Byte> chars = new ArrayList<Byte>(26);
            byte prev = 0;
            for (byte b : raw) {
                if (prev != b) {
                    chars.add(b);
                    prev = b;
                }
            }

            for (int index = 0; index < word.length; index++) {
                word[index] = (byte)chars.indexOf(word[index]);
            }

            iterate(word);
            for (int index = 0; index < word.length; index++) {
                word[index] = chars.get(word[index]);
            }
            System.out.println(new String(word, "US-ASCII"));
        }
    }

    private static boolean iterate(byte[] word) {
        for (int index = word.length - 2; index >= 0; index--) {
            if (word[index] < word[index + 1]) {
                int next = 0;
                int min = Integer.MAX_VALUE;
                for (int i = index + 1; i < word.length; i++) {
                    if (word[i] > word[index] && word[i] < min) {
                        next = i;
                        min = word[i];
                    }
                }

                byte temp = word[index];
                word[index] = word[next];
                word[next] = temp;

                Arrays.sort(word, index + 1, word.length);
                break;
            }
        }

        return true;
    }
}
