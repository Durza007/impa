import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Patrik on 2014-10-22.
 */
class SpeedyEscape {
    private final static int INF = 999999;

    private static class Path implements Comparable<Path> {
        final int intersection;
        final int distance;
        final double speed;

        private Path(int intersection, int distance, double speed) {
            this.intersection = intersection;
            this.distance = distance;
            this.speed = speed;
        }

        @Override
        public int compareTo(Path o) {
            return Double.compare(this.speed, o.speed);
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/SpeedyEscape.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            String params[] = input.readLine().split(" ");

            final int n = Integer.parseInt(params[0]);
            final int m = Integer.parseInt(params[1]);
            final int e = Integer.parseInt(params[2]);

            boolean[] isExit = new boolean[n];
            int[][] roads = new int[n][n];
            for (int[] r : roads) {
                Arrays.fill(r, INF);
            }

            LinkedList<Integer>[] nodes = new LinkedList[n];
            for (int i = 0; i < n; i++) {
                nodes[i] = new LinkedList<Integer>();
            }

            for (int i = 0; i < m; i++) {
                String[] road = input.readLine().split(" ");

                final int a = Integer.parseInt(road[0]) - 1;
                final int b = Integer.parseInt(road[1]) - 1;
                final int l = Integer.parseInt(road[2]);

                roads[a][b] = l;
                roads[b][a] = l;

                nodes[a].add(b);
                nodes[b].add(a);
            }

            String[] exits = input.readLine().split(" ");
            for (int i = 0; i < e; i++) {
                final int exit = Integer.parseInt(exits[i]) - 1;
                isExit[exit] = true;
            }

            String[] starts = input.readLine().split(" ");
            final int b = Integer.parseInt(starts[0]) - 1;
            final int p = Integer.parseInt(starts[1]) - 1;

            /*for (int i = 0; i < n; i++) {
                if (i == p) continue;
                roads[i][p] = INF;
                roads[p][i] = INF;
            }*/

            int[][] oldRoads = new int[n][n];
            for (int r = 0; r < n; r++) {
                System.arraycopy(roads[r], 0, oldRoads[r], 0, n);
            }

            // Some Floyd–Warshall!
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        if (roads[i][j] > roads[i][k] + roads[k][j]) {
                            roads[i][j] = roads[i][k] + roads[k][j];
                        }
                    }
                }
            }

            /*boolean possible = false;
            for (int i = 0; i < n; i++) {
                if (isExit[i]) {
                    if (roads[b][i] != INF) {
                        possible = true;
                        break;
                    }
                }
            }

            if (!possible) {
                System.out.println("IMPOSSIBLE");
                continue;
            }*/

            double[] timeLimits = new double[n];
            for (int i = 0; i < n; i++) {
                if (i == p) continue;

                timeLimits[i] = (roads[p][i] * 360.0) / 160.0;
            }

            Queue<Path> q = new PriorityQueue<Path>();
            q.add(new Path(b, 0, 0.0));

            double minSpeed = Double.POSITIVE_INFINITY;
            double[] speeds = new double[n];
            int[] distance = new int[n];
            Arrays.fill(speeds, Double.POSITIVE_INFINITY);
            Arrays.fill(distance, INF);
            speeds[b] = 0.0;
            distance[b] = 0;

            while (!q.isEmpty()) {
                Path path = q.poll();
                if (path.speed == Double.POSITIVE_INFINITY) continue;

                if (isExit[path.intersection]) {
                    minSpeed = Math.min(minSpeed, path.speed);
                } else {
                    for (int node : nodes[path.intersection]) {
                        int altDistance = distance[path.intersection] + oldRoads[path.intersection][node];
                        double alt = altDistance / timeLimits[node];
                        alt = Math.max(alt, path.speed);
                        if (Double.compare(alt, speeds[node]) < 0) {
                            speeds[node] = alt;
                            distance[node] = altDistance;
                            q.add(new Path(node, altDistance, alt));
                        }
                    }
                }
            }

            /*   1  function Dijkstra(Graph, source):
                 2      dist[source]  := 0                     // Distance from source to source
                 3      for each vertex v in Graph:            // Initializations
                 4          if v ≠ source
                 5              dist[v]  := infinity           // Unknown distance function from source to v
                 6              previous[v]  := undefined      // Previous node in optimal path from source
                 7          end if
                 8          add v to Q                         // All nodes initially in Q (unvisited nodes)
                 9      end for
                10
                11      while Q is not empty:                  // The main loop
                12          u := vertex in Q with min dist[u]  // Source node in first case
                13          remove u from Q
                14
                15          for each neighbor v of u:           // where v has not yet been removed from Q.
                16              alt := dist[u] + length(u, v)
                17              if alt < dist[v]:               // A shorter path to v has been found
                18                  dist[v]  := alt
                19                  previous[v]  := u
                20              end if
                21          end for
                22      end while
                23      return dist[], previous[]
                24  end function*/

            if (minSpeed == Double.POSITIVE_INFINITY) {
                System.out.println("IMPOSSIBLE");
            } else {
                System.out.println(minSpeed * 360);
            }
        }
    }
}
