import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Patrik on 2014-10-20.
 */
class Unixls {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/Unixls.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while ((line = input.readLine()) != null) {
            final int n = Integer.parseInt(line);

            String[] names = new String[n];
            for (int i = 0; i < n; i++) {
                names[i] = input.readLine();
            } //a         b
            //  a           b
            Arrays.sort(names);
            int longest = 0;
            for (String name : names) {
                longest = Math.max(longest, name.length());
            }

            for (int wordsPerColumn = 1; wordsPerColumn <= names.length; wordsPerColumn++) {
                final int columnCount = (int)Math.ceil(names.length / (double)wordsPerColumn);

                int width = (longest + 2) * columnCount - 2;
                if (width <= 60) {
                    StringBuilder sb = new StringBuilder();
                    System.out.println("------------------------------------------------------------");
                    for (int row = 0; row < wordsPerColumn; row++) {
                        for (int column = 0; column < columnCount; column++) {
                            if (row + column * wordsPerColumn >= names.length) break;
                            sb.append(padRight(names[row + column * wordsPerColumn], longest + 2));
                        }
                        sb.deleteCharAt(sb.length() - 1);
                        sb.deleteCharAt(sb.length() - 1).append('\n');
                    }

                    System.out.print(sb.toString());
                    break;
                }
            }
        }
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }
}
