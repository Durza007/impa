import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Patrik on 2014-10-13.
 */
class UbiquitousReligions {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/UbiquitousReligions.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        long testCase = 1;
        String line;
        while ((line = input.readLine()) != null) {
            String[] params = line.split(" ");

            final int n = Integer.parseInt(params[0]);
            final int m = Integer.parseInt(params[1]);

            if (n == 0 && m == 0) break;

            LinkedList<Integer>[] links = new LinkedList[n];
            for (int i = 0; i < n; i++) {
                links[i] = new LinkedList<Integer>();
            }

            for (int i = 0; i < m; i++) {
                String[] pair = input.readLine().split(" ");

                final int a = Integer.parseInt(pair[0]) - 1;
                final int b = Integer.parseInt(pair[1]) - 1;

                if (a == b) continue;
                links[a].add(b);
                links[b].add(a);
            }

            boolean[] used = new boolean[n];
            Stack<Integer> s = new Stack<Integer>();
            int count = 0;
            for (int i = 0; i < n; i++) {
                if (used[i] == false) {
                    count++;
                    s.push(i);
                    while (!s.empty()) {
                        int v = s.pop();
                        if (used[v]) continue;
                        used[v] = true;
                        for (int u : links[v]) {
                            s.push(u);
                        }
                    }
                }
            }

            System.out.println(String.format("Case %d: %d", testCase++, count));
        }
    }
}
