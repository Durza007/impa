import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Patrik on 2014-10-11.
 */
class CopyingBooksTest {
    public static void main(String args[]) throws IOException, InterruptedException {
        Random r = new Random();

        test(12, 6, new int[] {96, 34, 1, 75, 7, 91, 11, 24, 95, 45, 90, 11});

        test(17, 9, new int[]{9, 10, 87, 32, 12, 20, 43, 31, 80, 4, 38, 59, 27, 74, 17, 79, 98});

        test(11, 7, new int[]{100, 48, 16, 48, 38, 14, 72, 35, 99, 92, 73});

        test(9, 7, new int[]{42, 60, 88, 6, 37, 58, 85, 60, 70});

        test(9, 6, new int[] {98, 11, 41, 25, 84, 29, 73, 74, 58});

        test(10, 6, new int[] {6, 45, 33, 15, 80, 4, 19, 4, 93, 58});

        test(9, 5, new int[] {35, 75, 43, 61, 6, 43, 9, 78, 3});

        int m = 6;
        int k = 3;
        int[] books = new int[] {80, 55, 18, 99, 84, 99};

        test(m, k, books);

        m = 6;
        k = 3;
        books = new int[] {66, 52, 97, 91, 60, 21};
        test(m, k, books);

        test(6, 6, new int[]{88, 60, 38, 53, 98, 7});

        test(6, 3, new int[]{16, 76, 81, 5, 3, 6});

        test(7, 3, new int[]{20, 75, 58, 84, 7, 89, 5});

        test(6, 5, new int[]{79, 2, 98, 75, 78, 95});

        test(6, 5, new int[]{84, 89, 4, 82, 25, 98});

        test(7, 4, new int[]{62, 29, 17, 22, 22, 92, 51});

        test(7, 5, new int[]{49, 69, 47, 51, 72, 81, 65});

        test(10, 7, new int[]{9, 69, 50, 72, 39, 74, 59, 22, 77, 56});

        test(7, 5, new int[] {65, 57, 5, 91, 62, 63, 92});

        test(20, 13, new int[] {48, 35, 97, 48, 56, 1, 54, 100, 17, 90, 9, 69, 20, 97, 4, 90, 40, 76, 65, 1});

        test(15, 8, new int[] {6, 17, 48, 21, 48, 30, 56, 39, 32, 7, 7, 12, 2, 39, 74});

        test(16, 13, new int[] {11, 43, 73, 26, 31, 48, 13, 41, 22, 85, 75, 12, 66, 70, 97, 6});

        int count = 0;
        while(true) {
            count++;
            m = r.nextInt(20) + 2;
            k = r.nextInt(m - 1) + 2;

            books = new int[m];
            for (int index = 0; index < m; index++) {
                books[index] = r.nextInt(100000) + 1;
            }

            test(m, k, books);

            if (count % 1000 == 0) {
                System.out.println(count);
            }
        }
    }

    public static void test(int m, int k, int[] books) {
        CopyingBooks.USE_BRUTE = true;
        String brute = CopyingBooks.solve(m, k, books);
        CopyingBooks.USE_BRUTE = false;
        String fast = CopyingBooks.solve(m, k, books);

        if (!brute.equals(fast)) {
            System.out.println("FAILED.");
            System.out.println("     Problem: m = " + m + ", k = " + k);
            System.out.println("            : " + Arrays.toString(books));
            System.out.println("    Expected: " + brute);
            System.out.println("         Got: " + fast);
        } else {
            System.out.println("Correct.");
        }
    }
}
