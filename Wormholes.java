import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Patrik on 2014-10-12.
 */
class Wormholes {
    private static class Edge {
        final int start;
        final int end;
        final int weight;

        public Edge(int start, int end, int weight) {
            this.start = start;
            this.end = end;
            this.weight = weight;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/Wormholes.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            String[] params = input.readLine().split(" ");

            final int n = Integer.parseInt(params[0]);
            final int m = Integer.parseInt(params[1]);

            int[] vertices = new int[n];
            for (int i = 1; i < vertices.length; i++) {
                vertices[i] = Integer.MAX_VALUE;
            }

            ArrayList<Edge> edges = new ArrayList<Edge>(2000);
            for (int i = 0; i < m; i++) {
                String[] wormParams = input.readLine().split(" ");
                final int x = Integer.parseInt(wormParams[0]);
                final int y = Integer.parseInt(wormParams[1]);
                final int t = Integer.parseInt(wormParams[2]);

                edges.add(new Edge(x, y, t));
            }

            for (int i = 0; i < n; i++) {
                for (Edge e : edges) {
                    if (vertices[e.start] + e.weight < vertices[e.end]) {
                        vertices[e.end] = vertices[e.start] + e.weight;
                    }
                }
            }

            boolean possible = false;
            for (Edge e : edges) {
                if (vertices[e.start] + e.weight < vertices[e.end]) {
                    possible = true;
                }
            }

            if (possible) {
                System.out.println("possible");
            } else {
                System.out.println("not possible");
            }
        }
    }
}
