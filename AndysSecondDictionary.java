import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by Patrik on 2014-10-13.
 */
class AndysSecondDictionary {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/AndysSecondDictionary.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<String> wordList = new ArrayList<String>();
        HashSet<String> usedWords = new HashSet<String>();

        String hyphenWord = null;
        String line;
        while ((line = input.readLine()) != null) {
            String[] words = filter(line.toLowerCase()).split(" ");

            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                if (hyphenWord != null) {
                    word = hyphenWord.substring(0, hyphenWord.length() - 1) + word;
                    hyphenWord = null;
                }

                if (word.isEmpty()) continue;

                if (i == words.length - 1 && word.charAt(word.length() - 1) == '-') {
                    hyphenWord = word;
                } else if (!usedWords.contains(word)) {
                    wordList.add(word);
                    usedWords.add(word);
                }
            }
        }

        Collections.sort(wordList);
        for (String word : wordList) {
            System.out.println(word);
        }
    }

    private static String filter(String word) {
        return word.replaceAll("[^a-z^-]"," ");
    }
}
