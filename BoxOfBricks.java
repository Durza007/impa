import java.io.*;
import java.util.Arrays;

class BoxOfBricks {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/BoxOfBricks.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        int[] stacks = new int[101];

        int currentSet = 1;
        while(true) {
            int n = Integer.parseInt(input.readLine());
            if (n == 0) break;

            String[] heights = input.readLine().split(" ");
            int sum = 0;
            for (int index = 0; index < heights.length; index++) {
                int height = Integer.parseInt(heights[index]);
                sum += height;
                stacks[height] += 1;
            }

            int average = sum / n;
            int moves = 0;
            for (int index = 0; index < average; index++) {
                moves += stacks[index] * (average - index);
            }

            System.out.println(String.format("Set #%d\nThe minimum number of moves is %d.\n", currentSet++, moves));
            Arrays.fill(stacks, 0);
        }
    }
}

/*import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;


class BoxOfBricks {
    public static void main(String[] args) throws FileNotFoundException {
        System.setIn(new FileInputStream("src/BoxOfBricks.txt"));
        Scanner sc = new Scanner(new BufferedReader(new InputStreamReader(System.in)));

        int n;
        int setNumber = 1;
        int minimumMoves;
        int sum;
        int averageHeight;
        StringBuilder sb = new StringBuilder();
        while (sc.hasNextInt()){
            n = sc.nextInt();
            if (n == 0)
                break;

            int[] h = new int[n];
            sum = 0;
            minimumMoves = 0;
            for(int i = 0; i < n; ++i){
                h[i] = sc.nextInt();
                sum += h[i];
            }

            averageHeight = sum / n;

            for(int i = 0; i < n; ++i){
                if (h[i] > averageHeight){
                    minimumMoves += h[i] - averageHeight;
                }
            }

            sb.append("Set #").append(setNumber).append("\n");
            sb.append("The minimum number of moves is ").append(minimumMoves).append(".\n");
            System.out.print(sb.toString());

            sb = new StringBuilder();
            sb.append("\n");
            setNumber++;
        }
        sc.close();
    }
}*/
