import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-07.
 */
class TheMailboxManufacturersProblem {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/TheMailboxManufacturersProblem.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            String[] params = input.readLine().split(" ");
            final int k = Integer.parseInt(params[0]);
            final int m = Integer.parseInt(params[1]);

            if (k == 1) {
                System.out.println(getNumCrackers(m));
            } else {
                int count = 0;

                int maxLimit = m;
                int max = getNumCrackers(m);
                for (int i = 0; i < (k - 1); i++) {
                    for (int d = m; d >= 1; d--) {
                        int value = getNumCrackers(d);
                        if (value < max / 2) {
                            maxLimit = d + 1;
                            count += maxLimit;
                            max = getNumCrackers(maxLimit);
                            break;
                        }
                    }
                }

                count += getNumCrackers(maxLimit);
                System.out.println(count + ", max: " + maxLimit);
            }
        }
    }

    private static int getNumCrackers(int m) {
        return (m * (m + 1) / 2);
    }

    private static int getNumCrackers(int m, int n) {
        return getNumCrackers(m) - getNumCrackers(n);
    }
}
