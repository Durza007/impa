import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Patrik on 2014-10-03.
 */
class Doublets {
    private final static byte A = 'a';

    private static class Word {
        final String word;
        Word parent;
        boolean searched;
        LinkedList<String> permutations = null;

        public Word(String word) {
            this.word = word;
            this.searched = false;
            this.parent = null;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/Doublets.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        // Read dictionary
        Hashtable<String, Word> words = new Hashtable<String, Word>(30000);
        {
            String word;
            while (!(word = input.readLine()).isEmpty()) {
                Word newWord = new Word(word);
                words.put(newWord.word, newWord);
            }
        }

        // Find path
        boolean firstTime = true;
        String command;
        while ((command = input.readLine()) != null) {
            String[] commands = command.split(" ");

            if (!firstTime) {
                System.out.println();
            } else {
                firstTime = false;
            }

            if (commands[0].equals(commands[1])) {
                System.out.println(commands[0]);
                System.out.println(commands[1]);
                continue;
            }

            Word start = words.get(commands[0]);
            Word end = words.get(commands[1]);

            if (end == null || start == null) {
                System.out.println("No solution.");
                continue;
            }

            long startTime = System.nanoTime();

            Queue<Word> q = new LinkedList<Word>();
            q.add(start);
            start.searched = true;
            start.parent = null;

            boolean solved = false;
            while (!q.isEmpty()) {
                Word word = q.poll();
                if (word == end) {
                    List<Word> solution = new LinkedList<Word>();

                    Word walk = end;
                    while (walk != null) {
                        solution.add(walk);
                        walk = walk.parent;
                    }

                    StringBuilder sb = new StringBuilder();
                    for (int index = solution.size() - 1; index >= 0; index--) {
                        sb.append(solution.get(index).word).append('\n');
                    }

                    System.out.print(sb.toString());
                    solved = true;
                    break;
                } else {

                    if (word.permutations == null) {
                        // TEST ALL POSSIBLE PERMUTATIONS OF THE WORD
                        word.permutations = new LinkedList<String>();
                        byte[] raw = word.word.getBytes();
                        for (int index = 0; index < raw.length; index++) {
                            final byte original = raw[index];
                            for (int c = 0; c < 26; c++) {
                                if (original == (A + c)) continue;

                                raw[index] = (byte) (A + c);
                                word.permutations.add(new String(raw));
                            }
                            raw[index] = original;
                        }

                        for (int index = 0; index < word.word.length(); index++) {
                            word.permutations.add(word.word.substring(0, index) + word.word.substring(index + 1));
                        }

                        if (word.word.length() < 16) {
                            for (int index = 0; index <= word.word.length(); index++) {
                                final String a = word.word.substring(0, index);
                                String b = "";
                                if (index < word.word.length()) {
                                    b = word.word.substring(index, word.word.length());
                                }

                                for (int c = 0; c < 26; c++) {
                                    word.permutations.add(a + (char) (A + c) + b);
                                }
                            }
                        }
                    }

                    for (String s : word.permutations) {
                        Word w = words.get(s);
                        if (w != null && !w.searched) {
                            w.searched = true;
                            w.parent = word;
                            q.add(w);
                        }
                    }
                }
            }

            System.out.println("T: " + ((System.nanoTime() - startTime) / 1000000000.0));

            if (!solved) {
                System.out.println("No solution.");
            }

            for (Word w : words.values()) {
                w.searched = false;
                w.parent = null;
            }
        }
    }
}
