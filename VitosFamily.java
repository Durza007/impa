import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-06.
 */
class VitosFamily {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/VitosFamily500.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        int testCases = Integer.parseInt(input.readLine());

        for (int test = 0; test < testCases; test++) {
            String[] data = input.readLine().split(" ");
            final int numRelatives = Integer.parseInt(data[0]);

            int min = Integer.MAX_VALUE;
            int max = 0;

            int[] numbers = new int[numRelatives];
            for (int index = 1; index < data.length; index++) {
                final int number = Integer.parseInt(data[index]);
                max = Math.max(max, number);
                min = Math.min(min, number);
                numbers[index - 1] = number;
            }

            int minSum = Integer.MAX_VALUE;
            for (int vitoNum = 0; vitoNum < numRelatives; vitoNum++) {
                int sum = 0;
                final int vitoStreet = numbers[vitoNum];
                for (int relative = 0; relative < numRelatives; relative++) {
                    sum += Math.abs(vitoStreet - numbers[relative]);
                }

                minSum = Math.min(minSum, sum);
            }

            System.out.println(minSum);
        }
    }
}
