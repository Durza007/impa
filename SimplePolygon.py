import random

file = open("SimplePolygon2.txt", "w")

file.write("1\n")
file.write("20000 ")
for x in range(0, 20000):
    endChar = " "
    if x == 19999:
        endChar = "\n"

    file.write(str(random.randint(-10000, 10000)) + " " + str(random.randint(-10000, 10000)) + endChar)


file.close()
