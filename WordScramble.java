import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-04.
 */
class WordScramble {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/WordScramble.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while ((line = input.readLine()) != null) {
            String[] words = line.split(" ");

            StringBuilder sb = new StringBuilder();
            for (String word : words) {
                for (int c = word.length() - 1; c >= 0; c--) {
                    sb.append(word.charAt(c));
                }
                sb.append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());
        }
    }
}
