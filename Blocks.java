import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Patrik on 2014-10-11.
 */
class Blocks {
    private static class BoxType implements Comparable<BoxType> {
        final int num;
        int sum;

        public BoxType(int num) {
            this.num = num;
            this.sum = 1;
        }

        @Override
        public int compareTo(BoxType o) {
            return o.sum - this.sum;
        }
    }

    private static class Range {
        final int start;
        final int end;

        public Range(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getValue() {
            return (int)Math.pow(end - start, 2);
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/Blocks.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());

        for (int test = 0; test < testCases; test++) {
            final int n = Integer.parseInt(input.readLine());

            String[] nums = input.readLine().split(" ");
            List<Integer> boxes = new LinkedList<Integer>();
            for (int index = 0; index < n; index++) {
                boxes.add(Integer.parseInt(nums[index]));
            }

            Hashtable<Integer, BoxType> boxTypes = new Hashtable<Integer, BoxType>(n);
            for (int box : boxes) {
                BoxType boxType = boxTypes.get(box);
                if (boxType != null) {
                    boxType.sum++;
                } else {
                    boxType = new BoxType(box);
                    boxTypes.put(box, boxType);
                }
            }

            int maxSum = 0;
            Enumeration<BoxType> boxEnum = boxTypes.elements();
            while (boxEnum.hasMoreElements()) {
                BoxType b = boxEnum.nextElement();
                maxSum += Math.pow(b.sum, 2);
            }

            Range range = findLargestRange(boxes);
            List<Integer> temp = new LinkedList<Integer>(boxes);
            int sum = 0;
            while (range.getValue() > 0) {
                sum += range.getValue();
                for (int index = range.start; index < range.end; index++) {
                    temp.remove(range.start);
                }

                range = findLargestRange(temp);
            }

            if (sum == maxSum) {
                System.out.println(maxSum);
                continue;
            }

            System.out.println(sum + ", " + getBestSum(boxes, 0));
        }
    }

    private static int getBestSum(List<Integer> boxes, int currentSum) {
        if (boxes.size() == 0) return currentSum;
        List<Range> ranges = getRanges(boxes);

        int max = currentSum;
        for (Range range : ranges) {
            List<Integer> updated = new LinkedList<Integer>(boxes);

            for (int index = range.start; index < range.end; index++) {
                updated.remove(range.start);
            }

            max = Math.max(max, getBestSum(updated, currentSum + range.getValue()));
        }

        return max;
    }

    private static List<Range> getRanges(List<Integer> boxes) {
        List<Range> result = new LinkedList<Range>();
        int currentStart = 0;
        int currentType = 0;
        for (int index = 0; index < boxes.size(); index++) {
            if (boxes.get(index) != currentType) {
                if (currentType != 0) {
                    result.add(new Range(currentStart, index));
                }

                currentType = boxes.get(index);
                currentStart = index;
            }
        }

        if (currentType != 0) {
            result.add(new Range(currentStart, boxes.size()));
        }

        return result;
    }

    private static Range findLargestRange(List<Integer> boxes) {
        int largestStart = 0;
        int largestEnd = 0;
        int largest = 0;
        int currentStart = 0;
        int currentType = 0;
        for (int index = 0; index < boxes.size(); index++) {
            if (boxes.get(index) != currentType) {
                int newSize = index - currentStart;
                if (currentType != 0 && newSize > largest) {
                    largestStart = currentStart;
                    largestEnd = index;
                    largest = newSize;
                }

                currentType = boxes.get(index);
                currentStart = index;
            }
        }

        if (currentType != 0) {
            int newSize = boxes.size() - currentStart;
            if (newSize > largest) {
                largestStart = currentStart;
                largestEnd = boxes.size();
            }
        }

        return new Range(largestStart, largestEnd);
    }
}
