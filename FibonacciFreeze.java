import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

/**
 * Created by Patrik on 2014-10-12.
 */
class FibonacciFreeze {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/FibonacciFreeze.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        BigInteger[] fib = new BigInteger[5001];
        fib[0] = BigInteger.ZERO;
        fib[1] = BigInteger.ONE;
        for (int i = 2; i < fib.length; i++) {
            fib[i] = fib[i - 1].add(fib[i - 2]);
        }

        String line;
        while ((line = input.readLine()) != null) {
            final int num = Integer.parseInt(line);

            System.out.println(String.format("The Fibonacci number for %d is %d", num, fib[num]));
        }
    }
}
