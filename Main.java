import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* Name of the class has to be "Main" only if the class is public. */
class Main
{
    public static void main (String[] args) throws java.lang.Exception
    {
        Scanner sc = new Scanner(new BufferedReader(new InputStreamReader(System.in)));

        long a,b;
        while (sc.hasNextLong()){
            a = sc.nextLong();
            b = sc.nextLong();
            System.out.println(Math.abs(a-b));
        }
        sc.close();
    }
}
