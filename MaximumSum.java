import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-04.
 */
class MaximumSum {
    private static byte[][] array;

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/MaximumSum100.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int n = Integer.parseInt(input.readLine());
        array = new byte[n][n];

        int count = 0;
        while (count < n*n) {
            String[] nums = input.readLine().split(" ");
            for (String num : nums) {
                if (num.isEmpty()) continue;

                array[count / n][count % n] = Byte.parseByte(num);
                count++;
            }
        }

        int[][] sum = new int[n][n];
        for (int j = 0; j < n; j++) {
            sum[0][j] = array[0][j];
            for (int i = 1; i < n; i++) {
                sum[i][j] = sum[i - 1][j] + array[i][j];
            }
        }

        int[] temp = new int[n];
        int max = Integer.MIN_VALUE;
        for (int x = 0; x < n; x++) {
            for (int y = x + 1; y < n; y++) {
                for (int i = 0; i < n; i++) {
                    temp[i] = sum[y][i] - sum[x][i];
                }

                max = Math.max(max, kadane(temp));
            }
        }

        System.out.println(max);
    }

    private static int kadane(int[] a) {
        int maxEnding = 0;
        int maxSoFar = 0;

        for (int x : a) {
            maxEnding = Math.max(0, maxEnding + x);
            maxSoFar = Math.max(maxSoFar, maxEnding);
        }

        return maxSoFar;
    }
}
