import java.io.*;
import java.util.Random;

/**
 * Created by Patrik on 2014-10-03.
 */
class DoubletsTest {
    private final static byte A = 'a';

    public static void main(String args[]) throws IOException {
        OutputStream output = new FileOutputStream("src/Doublets.txt");
        PrintStream printOut = new PrintStream(output);

        System.setOut(printOut);

        Random r = new Random();
        String base = "aaaaaaaaaaaaaaaa";
        byte[] raw = base.getBytes();
        System.out.print(base + "\n");

        for (int c = 1; c < 26; c++) {
            for (int i = 0; i < raw.length; i++) {
                raw[i] = (byte)(A + c);
                System.out.println(new String(raw));
            }
        }

        byte[] raw2 = "zzzzzzzzzzzzzzz".getBytes();
        for (int c = 24; c >= 0; c--) {
            for (int i = raw2.length - 1; i >= 0; i--) {
                raw2[i] = (byte)(A + c);
                System.out.println(new String(raw2));
            }
        }

        System.out.print("\nhexsanacla hejganalle\n");

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        printOut.close();
    }
}
