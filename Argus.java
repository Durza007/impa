import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;

/**
 * Created by Patrik on 2014-10-06.
 */
class Argus {
    private static class Task implements Comparable<Task> {
        final int qNum;
        final int period;
        int nextTime;

        public Task(int qNum, int period) {
            this.qNum = qNum;
            this.period = period;
            this.nextTime = period;
        }

        @Override
        public int compareTo(Task o) {
            if (this.nextTime == o.nextTime) {
                return this.qNum - o.qNum;
            } else {
                return this.nextTime - o.nextTime;
            }
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/Argus.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        PriorityQueue<Task> queue = new PriorityQueue<Task>(3000);

        String line;
        while ((line = input.readLine()).equals("#") == false) {
            String[] command = line.split(" ");

            Task task = new Task(Integer.parseInt(command[1]), Integer.parseInt(command[2]));
            queue.add(task);
        }

        final int k = Integer.parseInt(input.readLine());
        int count = 0;
        while (count < k) {
            Task t = queue.poll();
            System.out.println(t.qNum);

            t.nextTime += t.period;
            queue.add(t);
            count++;
        }
    }
}
