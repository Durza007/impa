import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Patrik on 2014-10-06.
 */
class Rendezvous {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/Rendezvous.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        int testCase = 1;
        while (true) {
            String[] sizes = input.readLine().split(" ");
            int n = Integer.parseInt(sizes[0]);
            int m = Integer.parseInt(sizes[1]);

            if (n == 0) break;

            String[] places = new String[n];
            for (int index = 0; index < places.length; index++) {
                places[index] = input.readLine();
            }

            int[][] paths = new int[n][n];
            for (int[] row : paths) {
                Arrays.fill(row, 9999);
            }

            for (int i = 0; i < n; i++) {
               paths[i][i] = 0;
            }

            for (int index = 0; index < m; index++) {
                String[] data = input.readLine().split(" ");
                final int i = Integer.parseInt(data[0]) - 1;
                final int j = Integer.parseInt(data[1]) - 1;
                int k = Integer.parseInt(data[2]);

                if (paths[i][j] != 0) {
                    k = Math.min(k, paths[i][j]);
                }

                paths[i][j] = k;
                paths[j][i] = k;
            }

            // Some Floyd–Warshall!
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        if (paths[i][j] > paths[i][k] + paths[k][j]) {
                            paths[i][j] = paths[i][k] + paths[k][j];
                        }
                    }
                }
            }

            int bestPlace = 0;
            int bestSum = Integer.MAX_VALUE;
            for (int place = 0; place < n; place++) {
                int sum = 0;
                for (int friend = 0; friend < n; friend++) {
                    if (place == friend) continue;

                    sum += paths[place][friend];
                }

                if (sum < bestSum) {
                    bestPlace = place;
                    bestSum = sum;
                }
            }

            System.out.println(String.format("Case #%d : %s", testCase++, places[bestPlace]));
        }
    }
}
