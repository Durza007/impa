import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-06.
 */
class BurgerTime {
    private static final int NOTHING = 0;
    private static final int DRUGS = 1;
    private static final int FOOD = 2;


    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/BurgerTime.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        int l;
        while ((l = Integer.parseInt(input.readLine())) != 0) {
            String road = input.readLine();

            int min = Integer.MAX_VALUE;
            byte lastSeen = NOTHING;
            int pos = 0;
            boolean done = false;
            for (int i = 0; i < road.length(); i++) {
                final char c = road.charAt(i);
                if (c == 'Z') {
                    System.out.println(0);
                    done = true;
                    break;
                } else if (c == 'D') {
                    if (lastSeen == FOOD) {
                        min = Math.min(min, i - pos);
                    }

                    lastSeen = DRUGS;
                    pos = i;
                } else if (c == 'R') {
                    if (lastSeen == DRUGS) {
                        min = Math.min(min, i - pos);
                    }

                    lastSeen = FOOD;
                    pos = i;
                }
            }

            if (done) continue;

            System.out.println(min);
        }
    }
}
