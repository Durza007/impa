import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Patrik on 2014-10-22.
 */
class AllergyTest {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/AllergyTest.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            final int k = Integer.parseInt(input.readLine());

            Integer[] length = new Integer[k];
            for (int i = 0; i < k; i++) {
                length[i] = Integer.parseInt(input.readLine());
            }

            Arrays.sort(length, Collections.reverseOrder());

            int total = 0;
            int firstOne;
            for (firstOne = k - 1; firstOne >= 0; firstOne--) {
                if (length[firstOne] != 1) break;

                total++;
            }
            firstOne++;

            List<Integer> roomLeft = new LinkedList<Integer>();
            for (int index = 0; index < firstOne - 1; index += 2) {
                total += length[index] + 1;
                int left = length[index] - length[index + 1];
                if (left > 0) roomLeft.add(left);
            }

            if (firstOne % 2 == 1) {
                if (roomLeft.isEmpty()) {
                    total += length[firstOne - 1];
                } else {
                    int largest = 0;
                    boolean done = false;
                    for (int room : roomLeft) {
                        largest = Math.max(largest, room);

                        if (room >= (length[firstOne - 1] - 1)) {
                            total += 1;
                            done = true;
                            roomLeft.remove((Integer)room);
                            break;
                        }
                    }

                    if (!done) {
                        total += length[firstOne - 1] - largest;
                        roomLeft.remove((Integer)largest);
                    }
                }
            }

            while (roomLeft.size() >= 2) {
                int a = roomLeft.remove(0);
                int b = roomLeft.remove(0);

                total -= b;
            }

            System.out.println(total);
        }
    }
}
