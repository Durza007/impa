import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Patrik on 2014-10-06.
 */
class WhatsCryptanalysis {
    private static final byte A = (byte)'A';
    private static final byte Z = (byte)'Z';

    private static class Char implements Comparable<Char> {
        final int count;
        final byte letter;

        public Char(int count, byte letter) {
            this.count = count;
            this.letter = letter;
        }

        @Override
        public String toString() {
            return ((char)letter) + " " + count;
        }

        @Override
        public int compareTo(Char o) {
            return o.count - this.count;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/WhatsCryptanalysis.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int lines = Integer.parseInt(input.readLine());
        int[] numChars = new int[26];

        for (int index = 0; index < lines; index++) {
            String text = input.readLine().toUpperCase();

            for (int i = 0; i < text.length(); i++) {
                final byte c = (byte)text.charAt(i);

                if (c >= A && c <= Z) {
                    numChars[c - A]++;
                }
            }
        }

        Char[] chars = new Char[26];
        for (int index = 0; index < chars.length; index++) {
            chars[index] = new Char(numChars[index], (byte)(A + index));
        }

        Arrays.sort(chars);

        for (Char c : chars) {
            if (c.count > 0) {
                System.out.println(c);
            }
        }
    }
}
