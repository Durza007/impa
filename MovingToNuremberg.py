import random

file = open("MovingToNuremberg2.txt", "w")

n = 50000
file.write("1\n")
file.write(str(n) + "\n")
for x in range(1, n):
    file.write("1 " + str(x + 1) + " " + str(random.randint(1, 300)) + "\n")

m = random.randint(0, n)
avail = list(range(1, m + 1))
file.write(str(m) + "\n")

for i in range(0, m):
    i = random.randint(0, len(avail) - 1)
    file.write(str(avail[i]) + " " + str(random.randint(1, 500)) + "\n")
    del avail[i]


file.close()
