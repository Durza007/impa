import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-06.
 */
class PlayfairCipher {
    private static class Pos {
        final int row;
        final int column;

        public Pos(int row, int column) {
            this.row = row;
            this.column = column;
        }
    }

    private static final byte A = (byte)'A';
    private static final byte Q = (byte)'Q';

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/PlayfairCipher.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            String cipher = input.readLine().toUpperCase().replaceAll(" ", "");
            byte[][] cipherTable = new byte[5][5];

            byte[] cipherData = cipher.getBytes();
            boolean[] usedLetters = new boolean[25];
            int tableCount = 0;
            for (int index = 0; index < cipherData.length; index++) {
                final byte letter = cipherData[index];
                if (!usedLetters[i(letter)]) {
                    cipherTable[tableCount / 5][tableCount % 5] = letter;
                    usedLetters[i(letter)] = true;
                    tableCount++;
                }
            }

            // Fill remaining letters
            for (int index  = 0; index < 25; index++) {
                if (!usedLetters[index]) {
                    cipherTable[tableCount / 5][tableCount % 5] = (byte)g(index);
                    tableCount++;
                }
            }

            byte[] text = input.readLine().toUpperCase().replaceAll(" ", "").getBytes();
            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < text.length; index += 2) {
                // Check if we should add an X
                byte a = text[index];
                byte b;
                if (index == text.length - 1) {
                    b = 'X';
                } else if (a == text[index + 1]) {
                    b = 'X';
                    index--;
                } else {
                    b = text[index + 1];
                }

                Pos first = findLetter(cipherTable, a);
                Pos second = findLetter(cipherTable, b);

                if (first.row == second.row) {
                    sb.append((char)cipherTable[first.row][(first.column + 1) % 5]);
                    sb.append((char)cipherTable[second.row][(second.column + 1) % 5]);
                } else if (first.column == second.column) {
                    sb.append((char)cipherTable[(first.row + 1) % 5][first.column]);
                    sb.append((char)cipherTable[(second.row + 1) % 5][second.column]);
                } else {
                    sb.append((char)cipherTable[first.row][second.column]);
                    sb.append((char)cipherTable[second.row][first.column]);
                }
            }

            System.out.println(sb.toString());
        }
    }

    private static Pos findLetter(byte[][] table, byte letter) {
        for (int row = 0; row < 5; row++) {
            for (int column = 0; column < 5; column++) {
                if (table[row][column] == letter) {
                    return new Pos(row, column);
                }
            }
        }

        throw new RuntimeException("Could not find letter!");
    }

    private static int i(int letter) {
        if (letter > Q) {
            return letter - A - 1;
        } else {
            return letter - A;
        }
    }

    private static int g(int index) {
        if (index >= Q - A) {
            return index + A + 1;
        } else {
            return index + A;
        }
    }
}
