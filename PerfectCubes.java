import java.util.Hashtable;

/**
 * Created by Patrik on 2014-10-11.
 */
class PerfectCubes {
    public static void main(String args[]) {
        Hashtable<Integer, Integer> cubes = new Hashtable<Integer, Integer>();
        for (int i = 1; i <= 200; i++) {
            cubes.put((int)Math.pow(i, 3), i);
        }

        for (int a = 1; a <= 200; a++) {
            int value = (int)Math.pow(a, 3);
            for (int b = 2; b <= 200 ; b++) {
                for (int c = b; c <= 200; c++) {
                    int dCube = (int)(value - Math.pow(b, 3) - Math.pow(c, 3));
                    Integer d = cubes.get(dCube);
                    if (d != null && d >= c) {
                        System.out.println(String.format("Cube = %d, Triple = (%d,%d,%d)", a, b, c, d));
                    }
                }
            }
        }
    }
}
