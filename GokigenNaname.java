import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Patrik on 2014-10-22.
 */
class GokigenNaname {
    private static class Point {
        final int x;
        final int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private static class Constraint {
        final int ax;
        final int ay;
        final int bx;
        final int by;

        private Constraint(int ay, int ax, int by, int bx) {
            this.ax = ax;
            this.ay = ay;
            this.bx = bx;
            this.by = by;
        }

        @Override
        public boolean equals(Object other) {
            Constraint o = (Constraint)other;
            return this.ax == o.ax && this.ay == o.ay ||
                this.ax == o.bx && this.ay == o.by ||
                this.bx == o.ax && this.by == o.ay ||
                this.bx == o.bx && this.by == o.by;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/GokigenNaname.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            n = Integer.parseInt(input.readLine());

            int[] constraints = new int[(n + 1) * (n + 1)];
            for (int i = 0; i < n + 1; i++) {
                String row = input.readLine();

                for (int c = 0; c < n + 1; c++) {
                    if (row.charAt(c) != '.') {
                        constraints[ic(i, c)] = Integer.parseInt("" + row.charAt(c));
                    } else {
                        constraints[ic(i, c)] = -1;
                    }
                }
            }

            int[] board = new int[n * n];
            if (constraints[ic(0, 0)] < 2) {
                board[i(0, 0)] = constraints[ic(0, 0)] + 1;
            }
            if (constraints[ic(n, 0)] < 2) {
                board[i(n - 1, 0)] = constraints[ic(n, 0)] + 1;
            }
            if (constraints[ic(0, n)] < 2) {
                board[i(0, n - 1)] = constraints[ic(0, n)] + 1;
            }
            if (constraints[ic(n, n)] < 2) {
                board[i(n - 1, n - 1)] = constraints[ic(n, n)] + 1;
            }

            List<Constraint> extraConstraints = new LinkedList<Constraint>();

            // Check up, bottom, left and right constraints
            for (int i = 1; i < n; i++) {
                if (constraints[ic(0, i)] == 0) {
                    board[i(0, i - 1)] = 2;
                    board[i(0, i)] = 1;
                } else if (constraints[ic(0, i)] == 2) {
                    board[i(0, i - 1)] = 1;
                    board[i(0, i)] = 2;
                } else if (constraints[ic(0, i)] == 1) {
                    extraConstraints.add(new Constraint(0, i - 1, 0, i));
                }

                if (constraints[ic(n ,i)] == 0) {
                    board[i(n - 1, i - 1)] = 1;
                    board[i(n - 1, i)] = 2;
                } else if (constraints[ic(n ,i)] == 2) {
                    board[i(n - 1, i - 1)] = 2;
                    board[i(n - 1, i)] = 1;
                } else if (constraints[ic(n ,i)] == 1) {
                    extraConstraints.add(new Constraint(n - 1, i - 1, n - 1, i));
                }

                // Left
                if (constraints[ic(i, 0)] == 0) {
                    board[i(i - 1, 0)] = 2;
                    board[i(i, 0)] = 1;
                } else if (constraints[ic(i, 0)] == 2) {
                    board[i(i - 1, 0)] = 1;
                    board[i(i, 0)] = 2;
                } else if (constraints[ic(i, 0)] == 1) {
                    extraConstraints.add(new Constraint(i - 1, 0, i, 0));
                }

                // Right
                if (constraints[ic(i, n)] == 0) {
                    board[i(i - 1, n - 1)] = 1;
                    board[i(i, n - 1)] = 2;
                } else if (constraints[ic(i, n)] == 2) {
                    board[i(i - 1, n - 1)] = 2;
                    board[i(i, n - 1)] = 1;
                } else if (constraints[ic(i, n)] == 1) {
                    extraConstraints.add(new Constraint(i - 1, n - 1, i, n - 1));
                }
            }

            // Check 4 value constraint
            for (int y = 1; y < n; y++) {
                for (int x = 1; x < n; x++) {
                    if (constraints[ic(y, x)] == 4) {
                        board[i(y - 1, x - 1)] = 2;
                        board[i(y - 1, x)] = 1;
                        board[i(y, x - 1)] = 1;
                        board[i(y - 1, x - 1)] = 2;
                    }
                }
            }

            induceFromConstraints(board, constraints, extraConstraints);

            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < n; y++) {
                for (int x = 0; x < n; x++) {
                    char c = '.';
                    if (board[i(y, x)] == 1) {
                        c = '/';
                    } else if (board[i(y, x)] == 2) {
                        c = '\\';
                    }

                    sb.append(c);
                }
                sb.append('\n');
            }

            System.out.print(sb.toString());
        }
    }

    private static int n;
    private static int i(int y, int x) {
        return y * n + x;
    }

    private static int ic(int y, int x) {
        return y * (n + 1) + x;
    }

    private static void induceFromConstraints(int[] board, int[] constraints, List<Constraint> extra) {
        for (int y = 1; y < n - 1; y++) {
            for (int x = 2; x < n; x++) {
                if (constraints[ic(y, x)] == 1 && constraints[ic(y + 1, x - 1)] == 1) {
                    board[i(y, x - 1)] = 2;
                } else if (constraints[ic(y, x - 1)] == 1 && constraints[ic(y + 1, x)] == 1) {
                    board[i(y, x - 1)] = 1;
                }


            }
        }

        // Check horizontal 1-1 and 3-3
        for (int y = 1; y < n; y++) {
            for (int x = 1; x < n - 1; x++) {
                if (constraints[ic(y, x)] == 1 && constraints[ic(y, x + 1)] == 1 ||
                    constraints[ic(y, x)] == 3 && constraints[ic(y, x + 1)] == 3) {
                    extra.add(new Constraint(y - 1, x, y, x));
                }
            }
        }

        // Check vertical 1-1 and 3-3
        for (int y = 1; y < n - 1; y++) {
            for (int x = 1; x < n; x++) {
                if (constraints[ic(y, x)] == 1 && constraints[ic(y + 1, x)] == 1 ||
                    constraints[ic(y, x)] == 3 && constraints[ic(y + 1, x)] == 3) {
                    extra.add(new Constraint(y, x - 1, y, x));
                }
            }
        }
    }

    private static boolean search(int[] board, int[] constraints, LinkedList<Constraint> extra) {
        int[] copy = Arrays.copyOf(board, board.length);
        LinkedList<Constraint> newExtra = (LinkedList<Constraint>)extra.clone();

        if (newExtra.isEmpty()) {

        } else {
            LinkedList<Constraint> attached = new LinkedList<Constraint>();
            Constraint c = newExtra.remove(0);

            int i = -1;
            while ((i = newExtra.indexOf(c)) != -1) {
                attached.add(newExtra.remove(i));
            }

            //copy[i(c.ay, c.ax)] = 1;
            //copy[i(c.by, c.bx)] = c.isSame ? 1 : 2;
        }

        return false;
    }
}
