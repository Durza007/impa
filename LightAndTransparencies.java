import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Patrik on 2014-10-12.
 */
class LightAndTransparencies {
    private static class Plane {
        final String start;
        final String end;
        final double r;

        private Plane(String start, String end, double r) {
            this.start = start;
            this.end = end;
            this.r = r;
        }
    }

    private static class Point implements Comparable<Point> {
        final String point;
        final Plane owner;

        private Point(String point, Plane owner) {
            this.point = point;
            this.owner = owner;
        }

        @Override
        public int compareTo(Point o) {
            if (this.point.equals("-inf"))
                return -1;
            else if (this.point.equals("+inf"))
                return 1;
            else if (o.point.equals("-inf"))
                return 1;
            else if (o.point.equals("+inf"))
                return -1;

            int myPoint = this.point.indexOf('.');
            int oPoint = o.point.indexOf('.');
            if (myPoint != oPoint) {
                return myPoint - oPoint;
            }

            for (int i = 0; i < myPoint; i++) {
                if (this.point.charAt(i) != o.point.charAt(i)) {
                    return this.point.charAt(i) - o.point.charAt(i);
                }
            }

            int lim = Math.min(this.point.length(), o.point.length());
            for (int i = myPoint + 1; i < lim; i++) {
                if (this.point.charAt(i) != o.point.charAt(i)) {
                    return this.point.charAt(i) - o.point.charAt(i);
                }
            }

            return this.point.length() - o.point.length();
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/LightAndTransparencies.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            if (test != 0) System.out.println();

            input.readLine();

            final int n = Integer.parseInt(input.readLine());

            Plane[] planes = new Plane[n];
            Point[] points = new Point[2 * n];
            for (int index = 0; index < n; index++) {
                String[] planeParams = input.readLine().split(" ");

                planes[index] = new Plane(
                    planeParams[0],
                    planeParams[2],
                    parseDouble(planeParams[4])
                );

                points[index * 2 + 0] = new Point(planes[index].start, planes[index]);
                points[index * 2 + 1] = new Point(planes[index].end, planes[index]);
            }

            Arrays.sort(points);

            List<Plane> segments = new LinkedList<Plane>();
            List<Plane> stack = new LinkedList<Plane>();

            String lastPoint = points[0].point;
            stack.add(points[0].owner);
            for (int i = 1; i < points.length; i++) {
                Point p = points[i];
                double totalR = 1.0;
                for (Plane plane : stack) {
                    totalR *= plane.r;
                }

                segments.add(new Plane(lastPoint, p.point, totalR));
                lastPoint = p.point;

                if (stack.contains(p.owner)) {
                    stack.remove(p.owner);
                } else {
                    stack.add(p.owner);
                }
            }

            if (segments.get(0).start.equals("-inf") == false) {
                segments.add(0, new Plane("-inf", segments.get(0).start, 1.0));
            }

            if (segments.get(segments.size() - 1).end.equals("+inf") == false) {
                segments.add(new Plane(segments.get(segments.size() - 1).end, "+inf", 1.0));
            }

            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
            otherSymbols.setInfinity("inf");
            DecimalFormat decimal = new DecimalFormat("0.000", otherSymbols);
            System.out.println(segments.size());
            for (Plane segment : segments) {
                System.out.println(String.format("%s %s %s",
                    formatValue(segment.start),
                    formatValue(segment.end),
                    decimal.format(segment.r)));
            }
        }
    }

    private static String formatValue(String v) {
        if (v.contains("inf")) return v;

        int point = v.indexOf('.') + 1;
        if (point == 0) {
            return v + ".000";
        } else if (v.length() - point > 3) {
            return v.substring(0, point + 3);
        } else {
            int pad = v.length() + (3 - v.length() + point);
            return String.format("%-" + pad + "s", v).replace(' ', '0');
        }
    }

    private static double parseDouble(String s) {
        if (s.contains("+inf"))
            return Double.POSITIVE_INFINITY;
        else if (s.contains("-inf"))
            return Double.NEGATIVE_INFINITY;
        else
            return Double.parseDouble(s);
    }
}
