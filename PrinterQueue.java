import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Patrik on 2014-10-05.
 */
class PrinterQueue {
    private static class Job {
        int prio;
        boolean isMyJob;

        public Job(int prio, boolean isMyJob) {
            this.prio = prio;
            this.isMyJob = isMyJob;
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/PrinterQueue.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());

        for (int test = 0; test < testCases; test++) {
            String[] params = input.readLine().split(" ");
            final int n = Integer.parseInt(params[0]);
            final int myPos = Integer.parseInt(params[1]);

            String[] linePrio = input.readLine().split(" ");
            Queue<Job> queue = new LinkedList<Job>();
            for (int index = 0; index < n; index++) {
                queue.add(new Job(Integer.parseInt(linePrio[index]), index == myPos));
            }

            // Solve
            int minutes = 0;
            while (true) {
                Job job = queue.poll();
                boolean print = true;
                for (Job j : queue) {
                    if (j.prio > job.prio) {
                        queue.add(job);
                        print = false;
                        break;
                    }
                }

                if (print) {
                    minutes++;
                    if (job.isMyJob) break;
                }
            }

            System.out.println(minutes);
        }
    }
}
