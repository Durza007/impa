import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by Patrik on 2014-10-19.
 */
class SimplePolygon {
    private static class Point implements Comparable<Point> {
        static Point base = null;
        final int index;
        final int x;
        final int y;

        private Point(int index, int x, int y) {
            this.index = index;
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Point o) {
            if (this.y == base.y && o.y == base.y) {
                return this.x - o.x;
            }
            int diff = Double.compare(Math.atan2(this.y - base.y, this.x - base.x), Math.atan2(o.y - base.y, o.x - base.x));
            if (diff == 0) {
                return Double.compare(Math.pow(base.x - this.x, 2) + Math.pow(base.y - this.y, 2),
                    Math.pow(base.x - o.x, 2) + Math.pow(base.y - o.y, 2));
            } else {
                return diff;
            }
        }
    }

    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/SimplePolygon.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            //long startT = System.nanoTime();

            String[] params = input.readLine().split(" ");

            final int n = Integer.parseInt(params[0]);
            Point[] points = new Point[n];
            for (int i = 0; i < n; i++) {
                final int x = Integer.parseInt(params[1 + i * 2 + 0]);
                final int y = Integer.parseInt(params[1 + i * 2 + 1]);

                points[i] = new Point(i, x, y);
            }

            Point lowest = points[0];
            for (Point p : points) {
                if (p.y < lowest.y) {
                    lowest = p;
                } else if (p.y == lowest.y && p.x < lowest.x) {
                    lowest = p;
                }
            }

            Point.base = lowest;
            Arrays.sort(points);

            double angle = Math.atan2(points[n - 1].y - lowest.y, points[n - 1].x - lowest.x);
            for (int i = n - 2; i >= 0; i--) {
                double otherAngle = Math.atan2(points[i].y - lowest.y, points[i].x - lowest.x);
                if (Double.compare(angle, otherAngle) != 0) {
                    if (i < n - 2) {
                        Point[] tmp = new Point[n - (i + 1)];
                        System.arraycopy(points, i + 1, tmp, 0, tmp.length);

                        Arrays.sort(tmp, new Comparator<Point>() {
                            @Override
                            public int compare(Point a, Point b) {
                                return Double.compare(Math.pow(Point.base.x - b.x, 2) + Math.pow(Point.base.y - b.y, 2),
                                    Math.pow(Point.base.x - a.x, 2) + Math.pow(Point.base.y - a.y, 2));
                            }
                        });

                        System.arraycopy(tmp, 0, points, i + 1, tmp.length);
                    }

                    break;
                }
            }

            StringBuilder sb = new StringBuilder();
            for (Point p : points) {
                sb.append(p.index).append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());

            //System.out.println("T: " + ((System.nanoTime() - startT) / 1000000000.0) + " s");
        }
    }
}
