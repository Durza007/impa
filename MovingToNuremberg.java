import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Patrik on 2014-10-19.
 */
class MovingToNuremberg {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/MovingToNuremberg.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        final int testCases = Integer.parseInt(input.readLine());
        for (int test = 0; test < testCases; test++) {
            final int n = Integer.parseInt(input.readLine());

            int[][] subways = new int[n][n];
            for (int[] row : subways) {
                Arrays.fill(row, 999999);
            }

            for (int i = 0; i < n - 1; i++) {
                String[] subParams = input.readLine().split(" ");

                final int a = Integer.parseInt(subParams[0]) - 1;
                final int b = Integer.parseInt(subParams[1]) - 1;
                final int t = Integer.parseInt(subParams[2]);

                subways[a][b] = t;
                subways[b][a] = t;
            }

            final int m = Integer.parseInt(input.readLine());
            int[][] wantsToGo = new int[m][2];

            for (int i = 0; i < m; i++) {
                String[] wantParams = input.readLine().split(" ");

                final int dest = Integer.parseInt(wantParams[0]) - 1;
                final int times = Integer.parseInt(wantParams[1]);

                wantsToGo[i][0] = dest;
                wantsToGo[i][1] = times;
            }

            // Some Floyd–Warshall!
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        if (subways[i][j] > subways[i][k] + subways[k][j]) {
                            subways[i][j] = subways[i][k] + subways[k][j];
                        }
                    }
                }
            }

            ArrayList<Integer> bestSubways = new ArrayList<Integer>();
            int bestSum = Integer.MAX_VALUE;
            for (int i = 0; i < n; i++) {
                int sum = 0;
                for (int[] otherPlace : wantsToGo) {
                    if (i == otherPlace[0]) continue;

                    sum += subways[i][otherPlace[0]] * 2 * otherPlace[1];
                }

                if (sum == bestSum) {
                    bestSubways.add(i);
                } else if (sum < bestSum) {
                    bestSum = sum;
                    bestSubways.clear();
                    bestSubways.add(i);
                }
            }

            Collections.sort(bestSubways);
            System.out.println(bestSum);
            StringBuilder sb = new StringBuilder();
            for (Integer place : bestSubways) {
                sb.append(place + 1).append(' ');
            }
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());
        }
    }
}
