import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Patrik on 2014-10-05.
 */
class OptimalParking {
    public static void main(String args[]) throws IOException {
        System.setIn(new FileInputStream("src/OptimalParking.txt"));
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        int testCases = Integer.parseInt(input.readLine());

        for (int test = 0; test < testCases; test++) {
            final int storeCount = Integer.parseInt(input.readLine());

            String[] stores = input.readLine().split(" ");
            int[] positions = new int[storeCount];

            for (int index = 0; index < storeCount; index++) {
                positions[index] = Integer.parseInt(stores[index]);
            }

            // Solve
            int min = Integer.MAX_VALUE;
            int max = 0;
            for (int index = 0; index < storeCount; index++) {
                min = Math.min(min, positions[index]);
                max = Math.max(max, positions[index]);
            }

            System.out.println((max - min) * 2);
        }
    }
}
